---
layout: handbook-page-toc
title: "Account Based Marketing"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# What is account based marketing?
Account-based marketing is a strategic approach to marketing based on account awareness in which an organization considers and communicates with individual prospect or customer accounts as markets of one.  Through a close alignment between sales and marketing we focus on target accounts that fit our ICP or ideal customer profile.  At GitLab, we are at the beginning of our account based marketing efforts and in the process of defining our ICP and aligning our target accounts based on those criteria.
## [Account Based Marketing Project](gitlab.com/gitlab-com/marketing/account-based-marketing)
## [ABM Issue Board](https://gitlab.com/groups/gitlab-com/-/boards/1409957?label_name[]=Account%20Based%20Marketing)

## As we develop and roll out our account based strategy, we are following this [30/60/90 day](https://docs.google.com/spreadsheets/d/1dkqlFPaLZ5w8jt8WjDULRatOWuPzfm0ou5hkqW94Ymw/edit#) plan. The ABM issue board is currently mapped out based on this launch plan.

### MVC1: MM account list-  As our first iteration (30 day plan), we are going to target in Demandbase an account list of roughly 1,000 midmarket accounts based on a target account list provided by the reps.

### MVC2: Enterprise plan- As our 2nd rollout (60 day plan) we will be choosing 1 rep and account per region (East, West & PubSec AMER, EMEA and APAC) for our launch into enterprise.

## Other definitions
#### Total addressable Market (TAM)-
Also called total available market, total addressable market references the revenue opportunity available for a product or service. TAM helps to prioritize business opportunities by serving as a quick metric of the underlying potential of a given opportunity
#### Ideal customer profile (ICP)- 
Ideal customer profile is a description of a company who is the best fit for our solution.  This can include firmographics, environmental and behavioral characteristics.  We use this profile to align our account based marketing efforts
#### Target accounts- 
Accounts that fit our ideal customer profile that we will focus our account base strategy on.  Target accounts are simply accounts that we would like to make customers
#### Tiered Accounts- 
Our account based strategy will include tiering our target accounts based on the following tiers:

**Tier 1**- 1:1 strategy or targeting these accounts as markets of one.  Accounts that fall into tier 1 will match 100% of our ICP criteria and have a marketing plan customized to their organization

**Tier 2**- 1:few Accounts that match most, but not all, of our ICP criteria.  These accounts will have a marketing plan based on firmographic or environmental behavior, but based on “like” targets i.e. not customized on a 1:1 basis

**Tier 3**- 1:many the remainder of our target accounts that we are marketing to but without the resources and customization of higher tiers.

## Tools we use

#### [Demandbase](https://www.demandbase.com/)- Demandbase is a targeting and personalization platform which we use to target online ads to companies that fit our ICP and tiered account criteria.  

#### [TOPO](https://topohq.com/)- a research and advisory firm used by companies to develop and orchestrate their account based strategy.  We will be following their model of account base orchestration plays.


