---
layout: job_family_page
title: "Recruiting Program Analyst"
---

The Recruiting Program Analyst works to streamline and automate processes that provide our highest return on investment (ROI). As an Analyst, they play a critical role within the recruiting team by ensuring our programs and partnerships are structured in an optimal way, data feeding decision making is accurate, and build out improved ways to ensure data quality throughout our systems and programs. The Recruiting Program Analyst is highly detail-oriented and capable of relaying information and recommendations to a variety of hiring teams across the company. The Analyst works to improve and optimize processes, project and program management solutions and support iteration to ensure recruiting team Key Performance Indicators (KPIs) and Performance Indicators (PIs) are met. The Analyst thrives in a fast-paced, innovative and collaborative environment, and are excited by the idea to make impactful data-driven decisions every day. The Recruiting Program Analyst reports to the Manager, Recruiting Operations.

## Responsibilities

* Scale and optimize all of GitLab’s recruiting systems and tools enabling the recruiting team to work at optimal operational excellence
* Provide metrics and analytical support that will support recruiting teams, hiring teams, and strategies
* Manage vendor relationships for the recruiting team
* Research potential new programs and services, coordinate demonstrations, determine comparison metrics as well as the implementation of chosen programs
* Lead the implementation and optimization of our ATS and other recruiting tools to ensure we capitalize on our investment
* Design and monitor key metrics to evaluate the effectiveness of GitLab's recruiting practices
* Assist with initiatives to enhance current programs 
* Support recommendations by leveraging data from our ATS and market research


## Requirements

* 7+ years of experience in Operations supporting Recruiting, Human Resources, or People Operations
* Experience building reports and dashboards in a data visualization tool
* Passionate about data, analytics, and automation. Experience cleaning and modeling large quantities of raw, disorganized data
* Experience with a variety of data sources
* Experience in a startup environment
* Experience working remotely
* Proven ability to multitask and prioritize workload
* Excellent communication and interpersonal skills
* Demonstrated ability to work in a team environment and work collaboratively across the organization
* Proficient in Google Suite
* Willingness to learn and use software tools including Git and GitLab, prior experience with GitLab is a plus.
* Organized, efficient, and proactive with a keen sense of urgency
* Ability to recognize and appropriately handle highly sensitive and confidential information
* Prior experience using an applicant tracking system (ATS), such as Greenhouse
* Prior experience using a human resources information system (HRIS), such as BambooHR, is a plus
* Share our values and work in accordance with those values


## Performance Indicators

* [Hires vs. Plan](https://about.gitlab.com/handbook/hiring/metrics/#hires-vs-plan)
* [Time to Offer Accept](https://about.gitlab.com/handbook/hiring/metrics/#time-to-offer-accept-days)


## Hiring Process 

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](https://about.gitlab.com/company/team/).

* Qualified candidates will be invited to schedule a 30 minute screening call with a Recruiting Manager
* Then, candidates will be invited to schedule a 30 minute interview with a Peer and a 30 minute interview with another Recruiting Manager
* Next, candidates will be invited to a 45 minute interview with the Hiring Manager
* Final, candidates will meet with the VP of Recruiting

As always, the interviews and screening call will be conducted via a [video call](https://about.gitlab.com/handbook/communication/#video-calls). See more details about our interview process [here](https://about.gitlab.com/handbook/hiring/interviewing/).
